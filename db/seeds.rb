# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?

Book.create! :title => "The Hunger Games", :description => "Awesome book", :author => "collins"
Book.create! :title => "Catching Fire", :description => "Awesome book", :author => "collins"
Book.create! :title => "Mockingjay", :description => "Awesome book", :author => "collins"
Book.create! :title => "Is Everyone Hanging out Without Me?", :description => "Awesome book", :author => "kaling"
Book.create! :title => "Are You There, Vodka? It's Me Chelsea", :description => "Awesome book", :author => "handler"
Book.create! :title => "Death Note", :description => "Awesome book", :author => "ohba"
Book.create! :title => "One Piece", :description => "Awesome book", :author => "oda"
Book.create! :title => "The Pelican Brief", :description => "Awesome book", :author => "grisham"
Book.create! :title => "A Time to Kill", :description => "Awesome book", :author => "grisham"
Book.create! :title => "Along Came a Spider", :description => "Awesome book", :author => "patterson"
Book.create! :title => "A Game of Thrones", :description => "Awesome book", :author => "martin"
Book.create! :title => "A Clash of Kings", :description => "Awesome book", :author => "martin"
Book.create! :title => "A Storm of Swords", :description => "Awesome book", :author => "martin"
Book.create! :title => "A Feast for Crows", :description => "Awesome book", :author => "martin"
Book.create! :title => "A Dance with Dragons", :description => "Awesome book", :author => "martin"
Book.create! :title => "The Silmarillion", :description => "Awesome book", :author => "tolkien"
Book.create! :title => "The NeverEnding Story", :description => "Awesome book", :author => "ende"
Book.create! :title => "The Lean Startup", :description => "Awesome book", :author => "ries"
Book.create! :title => "Hooked", :description => "Awesome book", :author => "eyal"
