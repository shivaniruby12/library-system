Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, :controllers => {:registrations => "registrations"}
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "home#index"

  require 'sidekiq/web'

  mount Sidekiq::Web, at: '/sidekiq'
  resources :books
end
