ActiveAdmin.register Book do
  permit_params :title, :description, :author, :user_id

  scope('all', default: true) { Book.all.order('created_at') }

  index do
    selectable_column
    id_column
    column :title
    column :description
    column :author
    actions
  end

  filter :title
  filter :description
  filter :author

  show do
    attributes_table :title, :description, :author
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :description
      f.input :author
    end
    f.actions
  end

  collection_action :showbooks, method: :get do
  end

  controller do
    def showbooks
      @books = Book.all
    end
  end

  action_item :showbooks, only: :show do
    link_to 'View books list', showbooks_admin_books_path
  end

end
