# frozen_string_literal: false

class HomeController < ApplicationController

  def index
  end
end
