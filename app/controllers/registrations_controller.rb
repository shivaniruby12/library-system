class RegistrationsController < Devise::RegistrationsController
  def new
    # super
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to new_user_session_path
    else
      render :new
    end
  end

  def update
    # super
    if @user.update(user_params)
      redirect_to root_path
    else
      render :edit
    end
  end

  private
    def user_params
      params.require(:user).permit(:name, :email, :phone, :password, :password_confirmation)
    end
end
