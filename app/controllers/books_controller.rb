class BooksController < ApplicationController
  before_action :set_book, only: [:show, :edit, :update, :destroy]
  after_action :verify_authorized, except: :index

  def index
    @books = Book.includes(:user)
    # @books = Book.joins(:user).where("books.author = ?", "LogBookDetailsAuthor")
    # @books = Book.eager_load(:user)
    # @books = Book.all
    authorize @books
  end

  def show
    authorize @book
  end

  def new
    @book = Book.new
    authorize @book
  end

  def create
    @book = current_user.books.new(book_params)
    authorize @book
    if @book.save
      redirect_to books_path
    else
      render :new
    end
  end

  def edit
    authorize @book
  end

  def update
    authorize @book
    if @book.update(book_params)
      redirect_to books_path
    else
      render :edit
    end
  end

  def destroy
    authorize @book
    @book.destroy
    redirect_to books_path
  end

  private
    def book_params
      params.require(:book).permit(:title, :description, :author)
    end

    def set_book
      @book = Book.find(params[:id])
    end
end
