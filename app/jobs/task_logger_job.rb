class TaskLoggerJob < ApplicationJob
  queue_as :default

  # before_perform :print_before_perform_message
  # after_perform :print_after_perform_message

  before_enqueue :print_before_enqueue_message
  after_enqueue :print_after_enqueue_message

  def perform
    puts "TaskLoggerJob is performed"
  end

  # def print_before_perform_message
  #   puts "Before perform"
  # end

  # def print_after_perform_message
  #   puts "After perform"
  # end

  def print_before_enqueue_message
    puts "Before enqueue"
  end

  def print_after_enqueue_message
    puts "After enqueue"
  end
end
