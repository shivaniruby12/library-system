class LogJob < ApplicationJob
  queue_as :default

  def perform(book)
    puts "Created a book with following attributes :: #{book.attributes}"
  end
end
