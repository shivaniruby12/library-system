class BookPolicy < ApplicationPolicy
  attr_reader :user, :book

  def initialize(user, book)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user
    @user = user
    @book = book
  end

  def index?
    user.has_role?(:admin) || user.has_role?(:user)
  end

  def show?
    index?
  end

  def edit?
    user.has_role?(:admin)
  end

  def new?
    edit?
  end

  def create?
    edit?
  end

  def update?
    edit?
  end

  def destroy?
    edit?
  end
end
