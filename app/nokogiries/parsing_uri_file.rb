require 'nokogiri'
require 'open-uri'
require 'pry'

doc = Nokogiri::HTML(URI.open('http://localhost:3000/books'))
# puts doc


doc1 = Nokogiri::HTML(URI.open('https://nokogiri.org/tutorials/installing_nokogiri.html'))
doc1.css('nav ul.menu li a', 'article h2').each do |link|
  # puts link.content
end

doc.xpath('//nav//ul//li/a', '//article//h2').each do |link|
  # puts link.content
end

doc.search('nav ul.menu li a', '//article//h2').each do |link|
  # puts link.content
end

# puts html_doc = Nokogiri::HTML("<html><body><h1>Mr. Belvedere Fan Club</h1></body></html>")

# puts xml_doc  = Nokogiri::XML("<root><aliens><alien><name>Alf</name></alien></aliens></root>")

puts doc = Nokogiri.XML('<foo><bar /></foo>')
