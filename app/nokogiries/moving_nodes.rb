require 'nokogiri'
require 'pry'

@doc = Nokogiri::HTML::DocumentFragment.parse <<-EOHTML
<body>
  <h1>Three's Company</h1>
  <div>A love triangle.</div>
</body>
EOHTML

h1  = @doc.at_css "h1"
div = @doc.at_css "div"
# h1.parent = div

# puts @doc.to_html
div.add_next_sibling(h1)

puts @doc.to_html
