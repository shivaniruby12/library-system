class Book < ApplicationRecord
  belongs_to :user, required: false
  default_scope -> { order(created_at: :asc) }

  after_create :log_book_details

  def log_book_details
    LogJob.perform_later(self)
  end
end
