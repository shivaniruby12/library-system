class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable, :trackable , :rememberable, :validatable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :confirmable
  has_many :books

  after_create :assign_default_role

  def assign_default_role
    self.add_role(:user) if self.roles.blank?
  end
end
