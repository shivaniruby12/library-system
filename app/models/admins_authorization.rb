class AdminsAuthorization < ActiveAdmin::AuthorizationAdapter

  def authorized?(action, subject = nil)
    case subject
    when normalized(subject)
      action == :update || action == :destroy ? user.admin_role.name == 'superadmin' : true
    else
      true
    end
  end
end
